class Persona:

    def __init__(self, madre=None, padre=None):
        self.madre = madre
        self.padre = padre
        self.edad = 0
        self.recuerdos = ["Nació"]
        self.vivo = True
        self.nombre = None
        self.apellido = None

    def cumplirAnios(self):
        if self.estaVivo():
            self.edad = self.edad + 1
            self.recuerdos.append("Cumplió años, su edad: " + str(self.edad))
    def estaVivo(self):
        return self.vivo

    def esHuerfano(self):
        if (self.padre == None and self.madre == None):
            self.huerfano = True
        elif (not self.madre.estaVivo() and not self.padre.estaVivo()):
            self.huerfano = True
        else:
            self.huerfano = False
        return self.huerfano
        
    def obtenerRecuerdos(self):
        return self.recuerdos

    def esHermanoDe(self, otra_persona):
        if self.madre == otra_persona.madre:
            self.hermano = True
        elif self.padre == otra_persona.padre:
            self.hermano = True
        else:
            self.hermano = False
        self.recuerdos.append("Alguien averiguo si era hermano")
        return self.hermano

    def morir(self):
        self.vivo = False
        self.recuerdos.append("Murió a la edad de " + str(self.edad))

    def bautizar(self, nombre, apellido):
        if self.nombre is None:
            self.nombre = nombre
            self.apellido = apellido
            self.recuerdos.append("Lo bautizaron con el nombre: " + self.nombre)
        else:
            self.recuerdos.append("Intenataron bautizarlo pero ya tenía nombre")
        return self.nombre

    def __str__(self):
        if self.nombre is not None:
            return self.nombre
        else:
            return 'Persona'

    def obtenerEdad(self):
        return self.edad

padre = Persona()
padre.bautizar("Jose","Garcia")
padre.cumplirAnios()
padre.cumplirAnios()
padre.morir()
padre.esHuerfano()
madre = Persona()
madre.bautizar("Josefa","Lopez")
madre.bautizar("jj","aa")
madre.cumplirAnios()
hijo = Persona(madre, padre)
hijo.bautizar("Pepito","Garcia Lopez")
hijo.cumplirAnios()
hija = Persona(madre)
hija.bautizar("Anita","Garcia Lopez")
hijo.cumplirAnios()
print(hijo)
print("es hermano de?", hija.esHermanoDe(hijo))
Nadie = Persona()
print(Nadie)
print('Nadie es huerfano? ',Nadie.esHuerfano())
print(padre)
print('Historia de ', padre.obtenerRecuerdos())
print(madre)
print('Historia de ', madre.obtenerRecuerdos())
print(hijo)
print('Historia de ', hijo.obtenerRecuerdos())
print(hija)
print('Historia de ', hija.obtenerRecuerdos())
