#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 18 20:44:53 2019
ejercicio para crear una red neuronal desde cero
@author: cris
"""
from decimal import *
from inspect import *

getcontext().prec = 3

class Neurona:

    def __init__(self, sesgo=0, funcion=lambda a:a, valor=0, valor_ent=0, ide = 0):
        self.sesgo = sesgo
        self.funcion = funcion
        self.valor = valor
        self.valor_ent = valor_ent
        self.ide = ide

    def calc_funcion(self):
        self.valor = self.funcion(self.valor_ent + self.sesgo)
        return self.valor

    def __repr__(self):
        #fun = str(inspect.getsourcelines(self.funcion)).split(',')[2].split("=")
        #func = fun[-1]
        return '(ide = {}, sesgo = {}, valor = {} )\n'.format(self.ide, self.sesgo, self.valor)

class Conexion:
    def __init__(self, origen, destino, peso):
        self.origen = origen
        self.destino = destino
        self.peso = peso

    def __repr__(self):
        return 'ori{}, dest{}, peso{} '.format(self.origen, self.destino, self.peso)


class Red:

    def __init__(self, cant_capas=3, cant_por_capa=[1, 2, 1], valores_neuronas_entrada=[1],funciones_activacion=[lambda a:a] * 4, sesgos=[0]*2, pesos=[1]*4):
        """
        cuando se ingresa la cantidad de capa 0 es las entradas
        y la última es las salidas
        """
        self.cant_capas = cant_capas
        self.cant_por_capa = cant_por_capa
        self.valores_neuronas_entrada = valores_neuronas_entrada
        self.funciones_activacion = funciones_activacion
        self.sesgos = sesgos
        self.pesos = pesos


    def crear_neuronas(self):
        """
        genera las neuronas
        las neuronas tienen 4 items sesgo, valor y funcion de activacion
        en las neuronas de entrada el sesgo debe ser 0  y el valor es igual al
        valor de entrada
        """
        self.neuronas = []
        ide = 0
        e = 0
        #neuronas de entrada
        fila = []
        for j in range(self.cant_por_capa[0]):
            ide = ide + 1
            fila.append(Neurona(valor=self.valores_neuronas_entrada[e],\
                                ide=ide))
            e = e + 1
        self.neuronas.append(fila)
        #neuronas intermedias
        n = 0
        for h in range(1,self.cant_capas-1):
            fila=[]
            for i in range(self.cant_por_capa[h]):
                ide = ide + 1
                fila.append(Neurona(sesgo=self.sesgos[n], \
                                    funcion=self.funciones_activacion[n],\
                                    ide=ide))
                n = n + 1
            self.neuronas.append(fila)
        #neuronas de salid
        fila = []
        for k in range(self.cant_por_capa[-1]):
            ide = ide + 1
            fila.append(Neurona(ide=ide))
        self.neuronas.append(fila)
        return self.neuronas

    def crear_conexiones(self):
        self.conexiones = []
        p = 0
        for i in range(1, self.cant_capas):
            fila = []
            for j in range(self.cant_por_capa[i]):
                for h in range(self.cant_por_capa[i-1]):
                    fila.append(Conexion(self.neuronas[i-1][h], \
                                         self.neuronas[i][j], self.pesos[p]))
                    p = p + 1
            self.conexiones.append(fila)
        return self.conexiones

    def calcular_red(self):
        """
        valor de neurona = función(sumatoria (la salida de la neurona anterior
                                              * el peso de la conexión))
        """
        for i in range(len(self.conexiones)):
            for j in range(len(self.conexiones[i])):
                self.conexiones[i][j].destino.valor_ent = \
                    Decimal(self.conexiones[i][j].destino.valor_ent) + \
                    Decimal(self.conexiones[i][j].origen.valor) * \
                    Decimal(self.conexiones[i][j].peso)
            for j in range(len(self.conexiones[i])):
                self.conexiones[i][j].destino.valor = \
                    self.conexiones[i][j].destino.calc_funcion()

    def mostrar_neuronas(self):
        for i in range(len(self.neuronas)):
            print('capa ', i)
            for j in range(len(self.neuronas[i])):
                print(self.neuronas[i][j])

    def toString(self):
        """mete la RN en una string"""
        cant_por_capas = ''
        cant_capas = ''
        neuras = ''
        describe_red = ''
        conex = ''
        for x in range(len(self.neuronas)):
            #neuras = neuras +'capa: {}\n'.format(x)
            for y in range(len(self.neuronas[x])):
                #fun = str(str(inspect.getsourcelines(self.neuronas[x][j].funcion)).split(',')[-3].split("="))
                #func = fun
                neuras = neuras + str(self.neuronas[x][y])
            neuras = neuras + '\n'
            cant_por_capas = cant_por_capas +'Cantidad en capa {}: {}\n'.format(x+1, y+1)
        cant_capas = cant_capas +'Cantidad de capas: {}\n'.format(x+1)
        describe_red = cant_capas + cant_por_capas 
        describe_red = describe_red + 'Neuronas \n'
        describe_red = describe_red + neuras

        for i in range(len(self.conexiones)):
            for j in range(len(self.conexiones[i])):
                conex = conex + str('origen {}, peso {}, destino {} \n'.\
                                    format(self.conexiones[i][j].origen.ide,\
                                           self.conexiones[i][j].peso,\
                                           self.conexiones[i][j].destino.ide))
        describe_red = describe_red + 'Conexiones \n'
        describe_red = describe_red + conex
        return describe_red

    def fromString(self, unaString):
        """se configura según el string"""
        
        
    def toFile(self, filename):
        """graba la RN en un archivo"""
        
    def fromFile(self, filename):
        """lee la RN desde un archivo y se configura"""

dic = {
       "cant_capas":3, 
       "cant_por_capa":[2, 3, 1],
       "valores_neuronas_entrada":[1]*2,
       "funciones_activacion":[lambda a:a, lambda a:a ,lambda a:a, lambda a:a,\
                               lambda a:a, lambda a:a ,lambda a:a] ,
       "sesgos":[0,0,0],
       "pesos":[1,1,1,1,1,1,1,1,1]
       }
print(dic['funciones_activacion'][0])
redes = [Red() ,
         Red(cant_capas=dic['cant_capas'], cant_por_capa=dic['cant_por_capa'],\
          valores_neuronas_entrada=dic['valores_neuronas_entrada'],\
          funciones_activacion=dic['funciones_activacion'],\
          sesgos=dic['sesgos'],\
          pesos=dic['pesos']),
        Red(cant_capas=4, cant_por_capa=[2, 3, 1, 1], 
          valores_neuronas_entrada=[1, 2],
          funciones_activacion=[lambda a:a] * 7,
          sesgos=[2, 4, -5, 8],
          pesos=[0.5, 1.3, 0.3, 0.3, 0.2, 1.1, -1, 1, 0.8, 1])
        ]
#
for red in redes:
    print("---------------- test -------------------")
    mis_neuronas = red.crear_neuronas()
    mis_conexiones = red.crear_conexiones()
    red.mostrar_neuronas()
    #print('conexiones\n', mis_conexiones)
    red.calcular_red()
    print('Red luego del calculo')
    #red.mostrar_neuronas()
    print(red.toString())

#neuTest = Neurona()
#print(neuTest)
