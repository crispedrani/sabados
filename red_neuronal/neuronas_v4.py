#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 18 20:44:53 2019
ejercicio para crear una red neuronal desde cero
@author: cris
"""

class Neurona:

    def __init__(self, bias=0, funcion=lambda a:a, valor=0, valor_ent=0):
        self.bias = bias
        self.funcion = funcion
        self.valor = valor
        self.valor_ent = valor_ent


    def calc_funcion(self):
        self.valor = self.funcion(self.valor_ent + self.bias)
        return self.valor

    def __repr__(self):
        return '(bias{}, ent{}, val{})'.format(self.bias, self.valor_ent, self.valor)

class Conexion:

    def __init__(self, origen, destino, peso):
        self.origen = origen
        self.destino = destino
        self.peso = peso
        
    def __repr__(self):
        return 'ori{}, dest{}, peso{} \n'.format(self.origen, self.destino, self.peso)


class Red:

    def __init__(self, cant_capas=3, cant_por_capa=[1, 2, 1], lista_parametros=[[1, lambda a:a]] * 4 ):
        """
        cuando se ingresa la cantidad de capa 0 es las entradas
        y la última es las salidas
        """
        self.cant_capas = cant_capas
        self.cant_por_capa = cant_por_capa
        self.lista_parametros = lista_parametros


    def crear_neuronas(self):
        """
        genera las neuronas
        las neuronas tienen tres items sesgo, valor y funcion de activacion
        en las neuronas de entradas el sesgo debe ser 0  y el valor la entrada
        """
        n = 0
        self.neuronas = []
        for i in range(self.cant_capas):
            fila = [] #fila para armar matriz de neuroras
            for j in range(self.cant_por_capa[i]):
                if i == 0:
                    fila.append(Neurona(bias=0, valor=self.lista_parametros[n][0]))
                else:
                    fila.append(Neurona(self.lista_parametros[n][0], \
                                         self.lista_parametros[n][1]))
                n = n + 1
            self.neuronas.append(fila)
        return self.neuronas

    def crear_conexiones(self, lista_pesos=[1] * 4):
        self.conexiones = []
        p = 0
        for i in range(1, self.cant_capas):
            fila = []
            for j in range(self.cant_por_capa[i]):
                for h in range(self.cant_por_capa[i-1]):
                    fila.append(Conexion(self.neuronas[i-1][h], \
                                         self.neuronas[i][j], lista_pesos[p]))
                    p = p + 1
            self.conexiones.append(fila)
        return self.conexiones

    def calcular_red(self):
        """
        valor de neurona = función(sumatoria (la salida de la neurona anterior
                                              * el peso de la conexión))
        """
        for i in range(len(self.conexiones)):
            for j in range(len(self.conexiones[i])):
                self.conexiones[i][j].destino.valor_ent = self.conexiones[i][j].destino.valor_ent + self.conexiones[i][j].origen.valor * self.conexiones[i][j].peso
            for j in range(len(self.conexiones[i])):
                self.conexiones[i][j].destino.valor = self.conexiones[i][j].destino.calc_funcion()

    def mostrar_neuronas(self):
        for i in range(len(self.neuronas)):
            print('capa ', i)
            for j in range(len(self.neuronas[i])):
                print(self.neuronas[i][j])


#red = Red(cant_capas=3, cant_por_capa=[2, 2, 1], lista_parametros=[[1,lambda a:a],[1,lambda a:a],[1,lambda a:a],[1,lambda a:a],[0, lambda a:a]] )
#red = Red(cant_capas=3, cant_por_capa=[1, 2, 1], lista_parametros=[[1,lambda a:a]]*4)
red = Red(cant_capas=3, cant_por_capa=[2, 3, 1], lista_parametros=[[1,lambda a:a]]*6)
mis_neuronas = red.crear_neuronas()
mis_conexiones = red.crear_conexiones(lista_pesos=[1] * 9)
#print('neuronas\n', mis_neuronas)
red.mostrar_neuronas()
#print('conexiones\n', mis_conexiones)
#red.mostrar_neuronas()
print('calculo')
red.calcular_red()
red.mostrar_neuronas()
#red.mostrar_neuronas()
#print('red ', red)
#print('neuronas\n', mis_neuronas)
#print('conexiones\n', mis_conexiones)
