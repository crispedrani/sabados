#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 18 20:44:53 2019
ejercicio para crear una red neuronal desde cero
@author: cris
"""

class Neurona:

    def __init__(self, bias, funcion, valor_ent = 0, valor = 0):
        self.bias = bias
        self.funcion = funcion
        self.valor_ent = valor_ent
    
    def calc_funcion(self):
        return self.funcion(self.valor_ent + self.bias)
    
    def __repr__(self):
        return '(bias{}, ent{})'.format(self.bias, self.valor_ent)

class Conexion:
    
    def __init__(self, peso, origen, destino):
        self.peso = peso
        self.origen = origen
        self.destino = destino
    
    def __str__(self):
        return 'Conector peso {}, origen {}, destino {} \n'.format(self.peso, self.origen, self.destino)
    def __repr__(self):
        return 'Conector peso {}, origen {}, destino {} \n'.format(self.peso, self.origen, self.destino)

class Red:
    
    def __init__(self, cant_capas=3, cant_por_capa=[1, 2, 1], lista_parametros=[[1,1]] * 4, fun_act=lambda a:a ):
        """
        cuando se ingresa la cantidad de capa 0 es las entradas 
        y la última es las salidas
        """
        self.cant_capas = cant_capas
        self.cant_por_capa = cant_por_capa
        self.lista_parametros = lista_parametros
        self.fun_act = fun_act
    
    
    def crear_neuronas(self):
        """
        genera las neuronas
        """
        #verefica datos coerentes numero de capas y cantidad de parámetros
        if sum(self.cant_por_capa) != len(self.lista_parametros):
            raise Exception('No coincide el numero de neuronas'
                            'con los parámetros ingresados')
        else:
            #crea las neuronas
            n = 0
            self.neuronas = []
            for i in range(self.cant_capas):
                fila = [] #fila para armar matriz de neuroras
                for j in range(self.cant_por_capa[i]):
                    fila.append(Neurona(self.lista_parametros[n][0], \
                                             self.fun_act))
                    n = n + 1
                self.neuronas.append(fila)
            
            return self.neuronas
        
    def crear_conexiones(self, lista_pesos=[1] * 4):
        self.conexiones = []
        p = 0
        for i in range(1, self.cant_capas):
            for j in range(self.cant_por_capa[i]):
                for h in range(self.cant_por_capa[i-1]):
                    self.conexiones.append([[i-1, h],[i, j], lista_pesos[p]])
                    p = p + 1
                    #print('neurona actual', i, j)
                    #print('anterior' , i-1, h)

        return self.conexiones
        #neuronas de salida

    def calcular_red(self):
        """
        valor de neurona = función(sumatoria (la salida de la neurona anterior 
                                              * el peso de la conexión))
        """
        for neu in self.neuronas[0]:
            neu.valor = 0
            neu.valor = neu.calc_funcion()
            print(0, neu)
        for i in range (1, len(self.neuronas)):
            for neu in self.neuronas[i]:
                print(i, neu)
#            print(self.conexiones[i-1].origen)
                #neu.valor_ent = 0
            #print(self.conexiones[i-1])
            #neu.valor_ent = neu.valor_ent + self.conexiones[i-1].peso * self.conexiones[i-1].origen.valor
#                print(i, i-1, conex)
            #neu.valor = neu.calc_funcion()

def funcion(x):
    return x

"""
neu1 = Neurona(0, funcion, 1)
neu2 = Neurona(0, funcion, 2)

print(neu1.calc_funcion(), neu2.calc_funcion())

con1 = Conector(3, neu1, neu2)

print(con1)
"""

red = Red()
mis_neuronas = red.crear_neuronas()
mis_conexiones = red.crear_conexiones()
print('neuronas\n', mis_neuronas)
print('conexiones\n', mis_conexiones)
#Calculando = red.calcular_red()