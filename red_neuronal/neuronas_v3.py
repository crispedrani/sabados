#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 18 20:44:53 2019
ejercicio para crear una red neuronal desde cero
@author: cris
"""

class Neurona:

    def __init__(self, bias, funcion, valor = 0):
        self.bias = bias
        self.funcion = funcion
        self.valor = valor


    def calc_funcion(self, valor_ent):
        self.valor = self.funcion(valor_ent + self.bias)
        return self.valor

    def __repr__(self):
        return '(bias{}, val{})'.format(self.bias, self.valor)

class Conexion:

    def __init__(self, peso, origen, destino):
        self.peso = peso
        self.origen = origen
        self.destino = destino

    def __str__(self):
        return 'Conector peso {}, origen {}, destino {} \n'.format(self.peso, self.origen, self.destino)

def __repr__(self):
        return 'Conector peso {}, origen {}, destino {} \n'.format(self.peso, self.origen, self.destino)

class Red:

    def __init__(self, cant_capas=3, cant_por_capa=[1, 2, 1], lista_parametros=[[1, lambda a:a]] * 4 ):
        """
        cuando se ingresa la cantidad de capa 0 es las entradas
        y la última es las salidas
        """
        self.cant_capas = cant_capas
        self.cant_por_capa = cant_por_capa
        self.lista_parametros = lista_parametros


    def crear_neuronas(self):
        """
        genera las neuronas
        """
        #verefica datos coherentes numero de capas y cantidad de parámetros
        if sum(self.cant_por_capa) != len(self.lista_parametros):
            raise Exception('No coincide el numero de neuronas'
                            'con los parámetros ingresados')
        else:
            #crea las neuronas
            n = 0
            self.neuronas = []
            for i in range(self.cant_capas):
                fila = [] #fila para armar matriz de neuroras
                for j in range(self.cant_por_capa[i]):
                    fila.append(Neurona(self.lista_parametros[n][0], \
                                             self.lista_parametros[n][1]))
                    n = n + 1
                self.neuronas.append(fila)
            return self.neuronas

    def crear_conexiones(self, lista_pesos=[1] * 4):
        self.conexiones = []
        p = 0
        for i in range(1, self.cant_capas):
            for j in range(self.cant_por_capa[i]):
                for h in range(self.cant_por_capa[i-1]):
                    self.conexiones.append([[i-1, h], [i, j], lista_pesos[p]])
                    p = p + 1
        return self.conexiones

    def calcular_red(self):
        """
        valor de neurona = función(sumatoria (la salida de la neurona anterior
                                              * el peso de la conexión))
        """
        for neu in self.neuronas[0]:
            neu.valor = neu.bias
            print(0, neu.valor, neu.bias)
        for i in range (1, len(self.neuronas)):
            for neu in self.neuronas[i]:
                valor_entr = 0
                for j in range(len(self.conexiones)):
                    print(j, self.conexiones[j], j)
                    valor_entr = valor_entr + self.neuronas[0][0].valor
                	neu.valor = neu.calc_funcion(valor_entr)
                	print(i, neu.valor, neu.bias)

    def mostrar_neuronas(self):
        for i in range(len(self.neuronas)):
            print('capa ', i)
            for j in range(len(self.neuronas[i])):
                for h in range(len(self.neuronas[j])):
                    print(self.neuronas[j][h])

def funcion(x):
    return x



red = Red()
mis_neuronas = red.crear_neuronas()
mis_conexiones = red.crear_conexiones()
print('neuronas\n', mis_neuronas)
print('conexiones\n', mis_conexiones)
red.mostrar_neuronas()
red.calcular_red()
#red.mostrar_neuronas()
print('red ', red)
print('neuronas\n', mis_neuronas)

