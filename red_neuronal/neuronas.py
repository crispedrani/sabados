#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 18 20:44:53 2019
ejercicio para crear una red neuronal desde cero
@author: cris
"""
from decimal import * 

getcontext().prec = 3

def identidad(x):
    return x

def doble(x):
    return x * 2

class Neurona:
    
    def __init__(self, sesgo=0, funcion=identidad, valor=0, valor_ent=0, ide = 0):
        self.sesgo = sesgo
        self.funcion = funcion
        self.valor = valor
        self.valor_ent = valor_ent
        self.ide = ide
        
    def calc_funcion(self):
        self.valor = self.funcion(self.valor_ent + self.sesgo)
        return self.valor

    def __repr__(self):
        return '(ide = {}, sesgo = {}, valor = {})\n'.format(self.ide, self.sesgo, self.valor)

class Conexion:
    def __init__(self, origen, destino, peso):
        self.origen = origen
        self.destino = destino
        self.peso = peso

    def __repr__(self):
        return 'ori{}, dest{}, peso{} '.format(self.origen, self.destino, self.peso)


class Red:

    def __init__(self, cant_capas, cant_por_capa, valores_neuronas_entrada, funciones_activacion, sesgos, pesos):
        """
        cuando se ingresa la cantidad de capa 0 es las entradas
        y la última es las salidas
        """
        self.cant_capas = cant_capas
        self.cant_por_capa = cant_por_capa
        self.valores_neuronas_entrada = valores_neuronas_entrada
        self.funciones_activacion = funciones_activacion
        self.sesgos = sesgos
        self.pesos = pesos


    def crear_neuronas(self):
        """
        genera las neuronas
        las neuronas tienen 4 items sesgo, valor y funcion de activacion
        en las neuronas de entrada el sesgo debe ser 0  y el valor es igual al
        valor de entrada
        """
        self.neuronas = []
        ide = 0
        e = 0
        #neuronas de entrada
        fila = []
        for j in range(self.cant_por_capa[0]):
            ide = ide + 1
            fila.append(Neurona(valor=self.valores_neuronas_entrada[e],\
                                ide=ide, sesgo=0, funcion=identidad(self.valores_neuronas_entrada[e]), valor_ent=0))
            e = e + 1
        self.neuronas.append(fila)
        #neuronas intermedias
        n = 0
        for h in range(1,self.cant_capas-1):
            fila=[]
            for i in range(self.cant_por_capa[h]):
                ide = ide + 1
                fila.append(Neurona(sesgo=self.sesgos[n], \
                                    funcion=self.funciones_activacion[n],\
                                    ide=ide))
                n = n + 1
            self.neuronas.append(fila)
        #neuronas de salid
        fila = []
        for k in range(self.cant_por_capa[-1]):
            ide = ide + 1
            fila.append(Neurona(ide=ide))
        self.neuronas.append(fila)
        return self.neuronas

    def crear_conexiones(self):
        self.conexiones = []
        p = 0
        for i in range(1, self.cant_capas):
            fila = []
            for j in range(self.cant_por_capa[i]):
                for h in range(self.cant_por_capa[i-1]):
                    fila.append(Conexion(self.neuronas[i-1][h], \
                                         self.neuronas[i][j], self.pesos[p]))
                    p = p + 1
            self.conexiones.append(fila)
        return self.conexiones

    def calcular_red(self):
        """
        valor de neurona = función(sumatoria (la salida de la neurona anterior
                                              * el peso de la conexión))
        """
        for i in range(len(self.conexiones)):
            for j in range(len(self.conexiones[i])):
                self.conexiones[i][j].destino.valor_ent = \
                self.conexiones[i][j].destino.valor_ent+ \
                self.conexiones[i][j].origen.valor * \
                self.conexiones[i][j].peso
                for j in range(len(self.conexiones[i])):
                    self.conexiones[i][j].destino.valor = \
                self.conexiones[i][j].destino.calc_funcion()

    def mostrar_neuronas(self):
        for i in range(len(self.neuronas)):
            print('capa ', i)
            for j in range(len(self.neuronas[i])):
                print(self.neuronas[i][j])

    def toString(self):
        """string que describe la red, capas,
        cantidad de neuronas, sesgos"""
        

    def fromString(self, unaString):
        """se configura según el string"""

    def toFile(self, filename):
        """graba la RN en un archivo"""

    def fromFile(self, filename):
        """lee la RN desde un archivo y se configura"""


#creo test
        
def relu(x):
    """ Función Relu"""
    if x < 0:
        return 0
    else:
        x >= 0
        return x
    
cant_capas = 3
cant_por_capa = [1, 2, 1]
valores_neuronas_entrada = [1]
funciones_activacion = [relu] *2
sesgos = [1,1]
pesos = [1,1,1,1]

mi_red = Red(cant_capas, cant_por_capa, valores_neuronas_entrada, funciones_activacion, sesgos, pesos)
mis_neuronas = mi_red.crear_neuronas()
mis_conexiones = mi_red.crear_conexiones
mi_red.mostrar_neuronas()
mi_red.calcular_red()


