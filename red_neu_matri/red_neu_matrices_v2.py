#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jun  9 20:15:27 2019

@author: cris
"""
import numpy as np


class Red:

    """crea la red, en base a los parámetros:
    cant_en_capa: cantidad de neuronas por capa
    es una lista de enteros, en el orden de las capas
    0 es la capa en entrada -1 es la capade salida.
    lista de sesgos, a partir de la capa 1, una lista por cada capa
    """

    def __init__(self, cant_en_capa, sesgos, pesos):
        self.cant_en_capa = cant_en_capa
        self.sesgos = sesgos
        self.pesos = pesos

    def iniciar_red(self):
        """crea la red en base a los paramentros de inicializacion"""
        self.red = {"neuronas": [np.array([0]*n) for n in self.cant_en_capa],
                    "sesgos": [np.array(i) for i in self.sesgos],
                    "pesos": [[np.array(j) for j in self.pesos]]}
        return self.red

    def f_relu(self, x):
        if x <= 0:
            return 0
        else:
            return x

    def cal_capa(self, f, X, W, B):
        """ Dadas la funcion de activacion
        las matrices de entrada X, pesos W y bias B
        calcula la matriz de salida Y para la capa
        """
        # vectoriazacion de la funcion f
        vf = np.vectorize(f)
        # Matriz de salida
        Y = vf(np.dot(X, W) + B)
        return Y

    def calc_red(self, entradas):
        #capa 0
        self.red['neuronas'][0] = np.array(entradas)
        #claculos de los valores de neuronas
        for i in range(len(self.red['neuronas'])-1):
            self.red['neuronas'][0][i + 1] = self.cal_capa(
                    X = self.red['neuronas'][i],
                    f = self.f_relu, 
                    B = self.red['sesgos'][i],
                    W = self.red['pesos'][0][i])
#            print(type(self.red['neuronas'][0][i]))
#            print(type(self.f_relu))
#            print(type(self.red['sesgos'][0][i]))
#            print(type(self.red['pesos'][0][i]))




    def __str__(self):
        return str(self.red)


mi_red = Red([2, 3, 1], [[1, 1, 1], [1]],
             [[[0, 1, 2], [10, 11, 12]],
              [[0], [1], [2]]])

mi_red.iniciar_red()
print(mi_red)
mi_red.calc_red([10, 20])
print(mi_red)


