import math

def es_primo(num):
    i = 2
    if num == 2:
        return True
    else:
        raiz_n = math.sqrt(num)
        while i <= raiz_n:
            if num % i == 0:
                return False
                break
            i = i + 1
    return True

def divisor_primo_mayor(numero = 13195):
    raiz = math.sqrt(numero)
    x = 2
    mayor_primo = 1
    while x <= raiz:
        if numero % x == 0:
            divisor = x
            otro_divisor = numero // x
            #print(divisor, otro_divisor)
            if es_primo(divisor):
                if divisor > mayor_primo:
                    mayor_primo = divisor
            #print(divisor, " es primo")
            if es_primo(otro_divisor):
                if otro_divisor > mayor_primo:
                    mayor_primo = otro_divisor
        x = x + 1

    if mayor_primo is 1:
        mayor_primo = numero

    return mayor_primo

 


print("Divisor primo mayor de 1: ", divisor_primo_mayor(1))
print("Divisor primo mayor de 2: ", divisor_primo_mayor(2))
print("Divisor primo mayor de 17: ", divisor_primo_mayor(17))
print("Divisor primo mayor de 24: ", divisor_primo_mayor(3))
print("Divisor primo mayor de default: ", divisor_primo_mayor())
print("Divisor primo mayor de 600851475143: ", divisor_primo_mayor(600851475143))
