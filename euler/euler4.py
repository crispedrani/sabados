def es_palin(num):
    """ Determina si un número es palíndromo"""
    num = str(num)
    #print(num)
    i = 0
    while i <= (len(num)//2):
        #print(num[i],num[-i-1])
        if num[i] == num[-i-1]:
            palin = True
        else:
            palin = False
            break
        i = i + 1
    return palin


def genera_palin():
    palin_mayor = 0
    for i in range(100, 999):
        for j in range(100, 999):
            h = i * j
            if es_palin(h):
                #print("{} es palindromo".format(h))
                if h > palin_mayor:
                    palin_mayor, num1, num2 = h, i, j
    return palin_mayor, num1, num2

a, b ,c =genera_palin()
print("{} es el palindromo mayor generado por 2 numeros de 3 cifras, {},{}".format(a, b ,c))
