import math
""" halla el numero menor divisible por todos los numeros de 1 a 20"""

def es_divis_todos(num):
    """verifica si un numero es divisible por todos los numeros
    del 1 al 10"""
    div = 1
    while div <= math.sqrt(num):
        if num % div != 0:
            divis_todos = False
            break
        else:
            divis_todos = True
        div = div + 1
    return divis_todos


menor_divisible = 0
nume = 1
while menor_divisible == 0:
    if es_divis_todos(nume):
        menor_divisible = nume
        break
    nume = nume + 1
print(menor_divisible)



print(es_divis_todos(100))
print(es_divis_todos(2520))

