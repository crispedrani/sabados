#numero = 24
#numero = 13195
numero = 600851475143

def esPrimo(num):
    x = 1
    while x <= num:
        if num == 1:
            primo = True
        else:
            if num % x == 0:
                un_divisor = x
                if un_divisor != 1 and un_divisor != num:
                    primo = False
                    break
                elif un_divisor == num:
                    primo = True
                    break
            
        x = x + 1
    return primo

for i in range(numero):
    if numero % (i+1) == 0:
        if esPrimo(i + 1):
            divisor_primo_mayor = i + 1

#print(esPrimo(8))
#print(esPrimo(7))
#print(esPrimo(82589933))
print(divisor_primo_mayor)


