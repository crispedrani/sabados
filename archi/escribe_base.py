#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 31 18:10:54 2019

@author: cris
"""

import pymssql


def es_separador(caracter):
    if caracter in [':', '/', ' ', '=']:
        return True
    else:
        return False


archi = open('Archivo para leer.txt', 'r')

for linea in archi:
    parametro = ''
    valor = ''
    i = 0

    while not es_separador(linea[i]):
        parametro = parametro + linea[i]
        i = i + 1

    if parametro.lower() in ['dni', 'nombre', 'apellido',
                             'direccion', 'distrito', 'sexo',
                             'programador']:
        while es_separador(linea[i]):
            i = i + 1
        valor = linea[i:]
        if parametro.lower() == 'dni':
            dni = valor
        elif parametro.lower() == 'nombre':
            nombre = parametro
        elif parametro.lower() == 'apellido':
            apellido = parametro
        elif parametro.lower() == 'direccion':
            direccion = parametro
        elif parametro.lower() == 'distrito':
            distrito = parametro
        elif parametro.lower() == 'sexo':
            sexo = parametro
        else:
            print('algo salio mal')
        programador = 'Cris'

conexion = pymssql.connect(server="monostematicos.com", user="hernan", password="mage", database="beta-ori_sistema")
cursor = conexion.cursor()
cursor.execute("EXEC InsertarPersonaEnPadron '{}','{}','{}','{}','{}','{}','{}'".format(dni, nombre, apellido, direccion, distrito, sexo, programador))
row = cursor.fetchone()
print(row)
conexion.close()
archi.close()
