"""Juego de ajedrez"""


class Pieza:
    """define una pieza de ajedrez genérica"""

    def __init__(self):
        self.color = None

    def __str__(self):
        return "Pieza genérica"

    def movimientos_posibles(self):
        """devuelve la lista de movimientos posibles"""
        movimientos = []
        return movimientos


class Coordenada:
    """ Permite crear coordena del tablero de ajedrez
        debe letra string en a y h y numemo enero ente 1 y 8
    """

    def __init__(self, letra, numero):
        self.letra = letra
        self.numero = numero

    def getLetra(self):
        return self.__letra

    def setLetra(self, letra):
        if letra == str(letra) and letra.lower() in ['a', 'b', 'c',
                                                     'd', 'e', 'f', 'h']:
            self.__letra = letra
        else:
            raise Exception('Se debe ingresar una letra entre a y h')

    letra = property(getLetra, setLetra)

    def getNumero(self):
        return self.__numero

    def setNumero(self, numero):
        if numero in [1, 2, 3, 4, 5, 6, 7, 8]:
            self.__numero = numero
        else:
            raise Exception('Se debe ingresar un numero entre 1 y 8')

    numero = property(getNumero, setNumero)

    def __str__(self):
        return self.letra + str(self.numero)

    def __repr__(self):
        return self.letra + str(self.numero)


class Tablero:
    """Muestra las piezas, controla las movidas"""

    def __init__(self):
        self.piezas = []

    def __str__(self):
        return "Tablero"

    def casilla(self, coordenada):
        """devuelve pieza, color o nada"""
        for item in self.piezas:
            if item[1] == coordenada:
                return item[0], item[2]
                break
            return None, None

    def piezas(self, pieza, posicion, color):
        """agrega piezas a la lista de piezas del tablero"""
        self.piezas.append((pieza, posicion, color))
        return self.piezas

    def iniciar_partida(self):
        """pone las fichas en su lugar en el tablero"""

    def siguiente_turno(self):
        """inicia el siguiente turno"""

    def movimiento_es_valido(self):
        """Devuelve verdadero si es válido o Falso si en invalido y la razón"""

    def mover(self, coordena_inicio, coordenada_fin):
        """ devuelve booleano y razón por lo invalidado"""


class Peon(Pieza):
    """mueve adelante y come en diagonal"""

    def movimientos_posibles(self, Coordenada=None):
        """Informa movimientos posibles"""





"""
t = Tablero()
coor = Coordenada("a", 1)
try:
    coor2 = Coordenada(3,"c")
except Exception as e:
    print("Falló con error: ", str(e))
print(t.casilla(coor))
p = Pieza
print("pieza ", p)
print("coordenada: ", coor)
print("tablero", t)
"""

coord = Coordenada('c', 4)
print(coord)
coord.letra = 'f'
print([coord, coord])
try:
    coord.numero = 9
except Exception as e:
    print("Falló con mensaje: ", str(e))
    
try:
    coord.letra = 'w'
except Exception as e:
    print("Falló con mensaje: ", str(e))

print(coord)
coord.numero = coord.numero + 1
print(coord)





