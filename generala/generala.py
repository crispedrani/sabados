import random

def tirar(cant_dados = 5):
    """ejecuta tirada de acuerdo a la cantidad
    deseada de datos"""
    tirada = []
    for i in range(cant_dados):
        tirada.append(random.randint(1,6))
    return tirada

def jugar():
    '''realiza la jugada de una persona '''
    dados = tirar(5)
    print("Su tirada es: ", dados)
    veces = 1
    while  veces < 3 and input("Desea tirar otra vez? s/n ") == "s" :
        print("Seleccione las posiciones, ej. 253")
        posiciones = input()
        nuevos_dados = tirar(len(posiciones))
        n = 0
        for i in posiciones:
            dados[int(i)-1] = nuevos_dados[n]
            n = n + 1
        print("Su tirada es: ", dados)
        veces = veces + 1
    print("Tu jugada final es: ", dados)
    suma = 0
    for x in dados:
        suma = suma + int(x)
    return suma
    

def jugador():
    nombre = input("ingrese nombre jugador: ")
    jugada = jugar()
    print(nombre,", tus puntos son: ",jugada)
    salida = []
    salida.append(jugada)
    salida.append(nombre)
    return salida

jugador1 = jugador()
jugador2 = jugador()

if jugador1[0] == jugador2[0]:
    print("Empataron con: ", jugador1[0], " puntos - ", jugador1)
elif jugador1[0] > jugador2[0]:
    print("Ganó jugador ", jugador1[1], " - ", jugador1)
else:
    print("Ganó jugador", jugador2[1], " - ", jugador2)
