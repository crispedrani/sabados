import random

def tirar(cant_dados = 5):
    """ejecuta tirada de acuerdo a la cantidad
    deseada de datos"""
    tirada = []
    for i in range(cant_dados):
        tirada.append(random.randint(1,6))
    return tirada

def jugar():
    """realiza la jugada de una persona"""
    dados = tirar(5)
    print("Su tirada es: ", dados)
    veces = 1
    while  veces < 3 and input("Desea tirar otra vez? s/n ") == "s" :
        print("Seleccione las posiciones, ej. 253")
        posiciones = input()
        nuevos_dados = tirar(len(posiciones))
        n = 0
        for i in posiciones:
            dados[int(i)-1] = nuevos_dados[n]
            n = n + 1
        print("Su tirada es: ", dados)
        veces = veces + 1
    print("Tu jugada final es: ", dados)
    return dados

def jugador():
    nombre = input("ingrese nombre jugador: ")
    jugada = jugar()
    puntos = calcula_puntos(jugada)
    print(nombre,", jugada: ",jugada)
    salida = {"nombre" : nombre, "jugada" : jugada, "puntos" : puntos}
    return salida

def calcula_puntos(dados):
    #recorre los dados y forma el vector de puntos
    puntos = [0,0,0,0,0,0]
    suma_cat = 0
    for dado in dados:
        puntos[int(dado)-1] = puntos[int(dado)-1] + 1

    #suma de puntos
    #puntaje en base a vector de puntos calcula el puntaje final
    if puntos.count(5) == 1:
        resultado = 8
        suma_cat = puntos.index(5) + 1 * 5
    elif puntos.count(1) == 5 and (puntos[0] == 0 or puntos[5] == 0):
        resultado = 7
    elif puntos.count(4) == 1:
        resultado = 6
        suma_cat = puntos.index(4) + 1 * 4
    elif puntos.count(3) == 1 and puntos.count(2) == 1:
        resultado = 5
        suma_cat = (puntos.index(3) + 1) * 3 + (puntos.index(2) + 1) *2
    elif puntos.count(3) == 1 and puntos.count(2) == 0:
        resultado = 4
        suma_cat = (puntos.index(3) + 1) * 3
    elif puntos.count(2) == 2:
        resultado = 3
    elif puntos.count(2) == 1 and puntos.count(3) == 0:
        resultado = 2
        suma_cat = (puntos.index(2) + 1) * 2
    else:
        resultado = 1
    return resultado, suma_cat


#def desempatar(jugador1={}, jugador2={}):
    #

#principal
jugador1 = jugador()
jugador2 = jugador()

print("jugador 1: ", jugador1["nombre"], " dados: ", jugador1["jugada"], " puntos: ", jugador1["puntos"])
print("jugador 2: ", jugador2["nombre"], " dados: ", jugador2["jugada"], " puntos: ", jugador2["puntos"])

if jugador1["puntos"] == jugador2["puntos"]:
    desempatar()
    print("empate")
elif jugador1["puntos"] > jugador2["puntos"]:
    print("Ganó ", jugador1["nombre"])
else:
    print("Ganó ", jugador2["nombre"])
