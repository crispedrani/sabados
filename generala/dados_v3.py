import random

def tirar(cant_dados = 5):
    """ejecuta tirada de acuerdo a la cantidad
    deseada de datos"""
    tirada = []
    for i in range(cant_dados):
        tirada.append(random.randint(1,6))
    return tirada

def jugar():
    """realiza la jugada de una persona"""
    dados = tirar(5)
    print("Su tirada es: ", dados)
    veces = 1
    while  veces < 3 and input("Desea tirar otra vez? s/n ") == "s" :
        print("Seleccione las posiciones, ej. 253")
        posiciones = input()
        nuevos_dados = tirar(len(posiciones))
        n = 0
        for i in posiciones:
            dados[int(i)-1] = nuevos_dados[n]
            n = n + 1
        print("Su tirada es: ", dados)
        veces = veces + 1
    print("Tu jugada final es: ", dados)
    return dados


def calcula_puntos(dados):
    #recorre los dados y forma el vector de puntos
    puntos = [0,0,0,0,0,0]
    suma_cat = 0

    for dado in dados:
        puntos[int(dado)-1] = puntos[int(dado)-1] + 1

    #puntaje en base a vector de puntos calcula el puntaje final
    if puntos.count(5) == 1:
        puntaje = 8
    elif puntos.count(1) == 5 and (puntos[0] == 0 or puntos[5] == 0):
        puntaje = 7
    elif puntos.count(4) == 1:
        puntaje = 6
    elif puntos.count(3) == 1 and puntos.count(2) == 1:
        puntaje = 5
    elif puntos.count(3) == 1 and puntos.count(2) == 0:
        puntaje = 4
    elif puntos.count(2) == 2:
        puntaje = 3
    elif puntos.count(2) == 1 and puntos.count(3) == 0:
        puntaje = 2
    else:
        puntaje = 1
    return puntaje, puntos


def jugador(es_prueba = False, jugada_forzada = None):
    if es_prueba:
        nombre = "anonimo"
        jugada = jugada_forzada

    else:
        nombre = input("ingrese nombre jugador: ")
        jugada = jugar()
    
    puntaje, puntos = calcula_puntos(jugada)
    print(nombre,", jugada: ",jugada)
    salida = {"nombre" : nombre, "jugada" : jugada, "puntaje" : puntaje, "puntos" : puntos}
    return salida

def ganador(jugador1 = {}, jugador2 = {}):
    if jugador1["puntaje"] > jugador2["puntaje"]:
        gano = 1
    elif jugador1["puntaje"] < jugador2["puntaje"]:
        gano = 2
    else:
        puntaje = jugador1["puntaje"]
        if puntaje == 8:
            if jugador1["puntos"].index(5) >  jugador2["puntos"].index(5):
                gano = 1
            elif jugador1["puntos"].index(5) <  jugador2["puntos"].index(5):
                gano = 2
            else:
                gano = 3
        elif puntaje == 7:
            if jugador1["puntos"][0] > jugador2["puntos"][0]:
                gano = 2
            elif jugador1["puntos"][0] < jugador2["puntos"][0]:
                gano = 1 
            else:
                gano = 3
        elif puntaje == 6:
            if jugador1["puntos"].index(4) > jugador2["puntos"].index(4):
                gano = 1
            elif jugador1["puntos"].index(4) < jugador2["puntos"].index(4):
                gano = 2
            else:
                if jugardor1["puntos"].index(1) > jugardor2["puntos"].index(1):
                    gano = 1
                elif jugardor1["puntos"].index(1) < jugardor2["puntos"].index(1):
                    gano = 2 
                else:
                    gano = 3
        elif puntaje == 5:
            if jugador1["puntos"].index(3) > jugador2["puntos"].index(3):
                gano = 1
            elif jugador1["puntos"].index(3) < jugador2["puntos"].index(3):
                gano = 2
            else:
                if jugador1["puntos"].index(2) > jugador2["puntos"].index(2):
                    gano = 1
                elif jugador1["puntos"].index(2) < jugador2["puntos"].index(2):
                    gano = 2
                else:
                    gano = 3
        elif puntaje == 4:
            if jugador1["puntos"].index(3) > jugador2["puntos"].index(3):
                gano = 1
            elif jugador1["puntos"].index(3) < jugador2["puntos"].index(3):
                gano = 2
            else:
                suma1 = jugador1["puntos"].index(1) 
                jugador1["puntos"].remove(1)
                suma1 = suma1 + jugador1.index(1)
                suma2 = jugador2["puntos"].index(1)
                jugador2["puntos"].remove(1)
                suma2 = suma2 + jugador2.index(1)
                if suma1 > suma2:
                    gano = 1
                elif suma1 < suma2:
                    gano = 2
                else:
                    gano = 3
        elif puntaje == 3:
            par11 = jugador1["puntos"].index(2)
            jugador1["puntos"].remove(2)
            par12 = jugador1["puntos"].index(2)
            par21 = jugador2["puntos"].index(2)
            jugador2["puntos"].remove(2)
            par22 = jugador2["puntos"].index(2)
            if par12 > par22:
                gano = 1
            elif par12 < par22:
                gano = 2
            else:
                if par11 > par21:
                    gano = 1
                elif par11 < par21:
                    gano = 2
                else:
                    if jugador1["puntos"].index(1) > jugador2["puntos"].index(1):
                        gano = 1
                    elif jugador1["puntos"].index(1) < jugador2["puntos"].index(1):
                        gano = 2
                    else:
                        gano = 3
        elif puntaje == 2:
            if jugador1["puntos"].index(2) >  jugador2["puntos"].index(2):
                gano = 1
            elif jugador1["puntos"].index(2) <  jugador2["puntos"].index(2):
                gano = 2
            else:
                suma1 = jugador1["puntos"].index(1)
                jugador1["puntos"].remove(1)
                suma1 = suma1 + jugador1.index(1)
                jugador1["puntos"].remove(1)
                suma1 = suma1 + jugador1.index(1)
                suma2 = jugador2["puntos"].index(1)
                jugador2["puntos"].remove(1)
                suma2 = suma2 + jugador2.index(1)
                jugador2["puntos"].remove(1)
                suma2 = suma2 + jugador2.index(1)
                if suma1 > suma2:
                    gano = 1
                elif suma1 < suma2:
                    gano = 2
                else:
                    gano = 3
        else:
            suma1, suma2 = 0, 0
            for x in jugador1["jugada"]:
                suma1 = suma1 + int(jugador1["jugada"][x])
            for y in jugador2["jugada"]:
                suma2 = suma2 + int(jugador2["jugada"][x])
                if suma1 > suma2:
                    gano = 1
                elif suma1 < suma2:
                    gano = 2
                else:
                    gano = 3

    return gano

#principal
if input("Desea ejecutar test? s/n ") == "s":
    jugador1 = jugador(es_prueba = True, jugada_forzada = [3,2,1,5,6] )
    jugador2 = jugador(es_prueba = True, jugada_forzada = [3,2,1,5,6] )
else: 
    jugador1 = jugador()
    jugador2 = jugador()
  
    print("jugador 1: ", jugador1["nombre"], " dados: ", jugador1["jugada"], " puntaje: ", jugador1["puntaje"], jugador1["puntos"])
    print("jugador 2: ", jugador2["nombre"], " dados: ", jugador2["jugada"], " puntaje: ", jugador2["puntaje"], jugador2["puntos"])
  
    gano = ganador(jugador1, jugador2)
    print("Ganador: ", gano)
