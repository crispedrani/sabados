import random

def tirar(cant_dados = 5):
    """ejecuta tirada de acuerdo a la cantidad
    deseada de datos"""
    tirada = []
    for i in range(cant_dados):
        tirada.append(random.randint(1,6))
    return tirada

def jugar():
    """realiza la jugada de una persona"""
    dados = tirar(5)
    print("Su tirada es: ", dados)
    veces = 1
    while  veces < 3 and input("Desea tirar otra vez? s/n ") == "s" :
        print("Seleccione las posiciones, ej. 253")
        posiciones = input()
        nuevos_dados = tirar(len(posiciones))
        n = 0
        for i in posiciones:
            dados[int(i)-1] = nuevos_dados[n]
            n = n + 1
        print("Su tirada es: ", dados)
        veces = veces + 1
    print("Tu jugada final es: ", dados)
    return dados

def jugador():
    nombre = input("ingrese nombre jugador: ")
    jugada = jugar()
    print(nombre,", jugada: ",jugada)
    salida = []
    salida.append(jugada)
    salida.append(nombre)
    return salida

def calcula_puntos(dados):
    #recorre los dados y forma el vector de puntos
    puntos = [0,0,0,0,0,0]
    for dado in dados:
        puntos[int(dado)-1] = puntos[int(dado)-1] + 1
    #puntaje en base a vector de puntos calcula el puntaje final
    if 5 in puntos:
        resultado = 8
    #elif [0,1,1,1,1,1] == puntos or [1,1,1,1,1,0] == puntos:
    elif puntos.count(1) == 5 and (puntos[0] == 0 or puntos[5] == 0):
        resultado = 7
    elif 4 in puntos:
        resultado = 6
    elif 3 in puntos and 2 in puntos:
        resultado = 5
    elif 3 in puntos and 2 not in puntos:
        resultado = 4
    elif puntos.count(2) == 2:
        resultado = 3
    elif puntos.count(2) == 1 and puntos.count(3) == 0:
        resultado = 2
    else:
        resultado = 1
    return resultado



jugador1 = jugador()
jugador2 = jugador()

puntos_1 = calcula_puntos(jugador1[0])
puntos_2 = calcula_puntos(jugador2[0])

print("jugador 1: ", jugador1, " puntos: ", puntos_1)
print("jugador 2: ", jugador2, " puntos: ", puntos_2)



#if jugador1[0] == jugador2[0]:
#    print("Empataron con: ", jugador1[0], " puntos - ", jugador1)
#elif jugador1[0] > jugador2[0]:
#    print("Ganó jugador ", jugador1[1], " - ", jugador1)
#else:
#    print("Ganó jugador", jugador2[1], " - ", jugador2)
