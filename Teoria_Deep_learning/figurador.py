#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec  2 22:36:16 2019

@author: cris
"""
from PIL import Image, ImageDraw
from math import fabs, atan, degrees
import random
import os

if not os.path.exists('Imagenes/'):
    os.makedirs('Imagenes/')
if not os.path.exists('Imagenes/blanco/'):
    os.makedirs('Imagenes/blanco/')
if not os.path.exists('Imagenes/negro/'):
    os.makedirs('Imagenes/negro/')
if not os.path.exists('Imagenes/blanco/Square/'):
    os.makedirs('Imagenes/blanco/Square/')
if not os.path.exists('Imagenes/negro/Square/'):
    os.makedirs('Imagenes/negro/Square/')
if not os.path.exists('Imagenes/blanco/Circle/'):
    os.makedirs('Imagenes/blanco/Circle/')
if not os.path.exists('Imagenes/negro/Circle/'):
    os.makedirs('Imagenes/negro/Circle/')
if not os.path.exists('Imagenes/blanco/Triangle/'):
    os.makedirs('Imagenes/blanco/Triangle/')
if not os.path.exists('Imagenes/negro/Triangle/'):
    os.makedirs('Imagenes/negro/Triangle/')
# tamaño de la imagen en pixels
size = (128, 128)

# Rectángulos y Circle
for i in range(15000):
    # valores al azar para coordenadas, tamaños y grosores
    x1 = random.randint(0, 128)
    x2 = random.randint(0, 128)
    x3 = random.randint(0, 128)
    y1 = random.randint(0, 128)
    y2 = random.randint(0, 128)
    y3 = random.randint(0, 128)
    w = random.randint(1, 15)

    # condicion para Square y Circle válidas
    if fabs(x2-x1)<128 and fabs(y2-y1)<128 and (x2-x1)>20 and (y2-y1)>20:
        # Rectángulos
        rec = Image.new('L', size, 255)
        draw = ImageDraw.Draw(rec, 'L')
        draw.rectangle(((x1, y1), (x2, y2)), fill=0, outline=0)
        rec.save('Imagenes/negro/Square/p{}.png'.format(i))

        recl = Image.new('L', size, 255)
        draw = ImageDraw.Draw(recl, 'L')
        draw.rectangle(((x1, y1), (x2, y2)), fill=255, outline=0)
        recl.save('Imagenes/negro/Square/pl{}.png'.format(i))

        reci = Image.new('L', size, 0)
        draw = ImageDraw.Draw(reci, 'L')
        draw.rectangle(((x1, y1), (x2, y2)), fill=255, outline=255)
        reci.save('Imagenes/blanco/Square/pi{}.png'.format(i))

        recli = Image.new('L', size, 0)
        draw = ImageDraw.Draw(recli, 'L')
        draw.rectangle(((x1, y1), (x2, y2)), fill=0, outline=255)
        recli.save('Imagenes/blanco/Square/pli{}.png'.format(i))

        # Circle
        eli = Image.new('L', size, 255)
        draw = ImageDraw.Draw(eli, 'L')
        draw.ellipse(((x1, y1), (x2, y2)), fill=0, outline=0)
        eli.save('Imagenes/negro/Circle/p{}.png'.format(i))

        elii = Image.new('L', size, 0)
        draw = ImageDraw.Draw(elii, 'L')
        draw.ellipse(((x1, y1), (x2, y2)), fill=255, outline=255)
        elii.save('Imagenes/blanco/Circle/pi{}.png'.format(i))

        elil = Image.new('L', size, 255)
        draw = ImageDraw.Draw(elil, 'L')
        draw.ellipse(((x1, y1), (x2, y2)), fill=255, outline=0)
        elil.save('Imagenes/negro/Circle/pl{}.png'.format(i))

        elili = Image.new('L', size, 0)
        draw = ImageDraw.Draw(elili, 'L')
        draw.ellipse(((x1, y1), (x2, y2)), fill=0, outline=255)
        elili.save('Imagenes/blanco/Circle/pli{}.png'.format(i))

    # condicion para triángulos válidos
    deno1 = (x1-x3)*(x1-x2)+(y1-y2)*(y1-y3)
    deno2 = (x1-x3)*(x3-x2)+(y3-y2)*(y1-y3)
    if deno1!=0 and deno2!=0:
        if ((15 < degrees(atan(((y1-y3)*(x1-x2)-(x1-x3)*(y1-y2))/deno1)) < 140)
            and
           (15 < degrees(atan(((y1-y3)*(x3-x2)-(x1-x3)*(y3-y2))/deno2)) < 140)):
            if  ((x1-x2)**2+(y1-y2)**2 > 400 and
                 (x3-x2)**2+(y3-y2)**2 > 400 and
                 (x1-x3)**2+(y1-y3)**2 > 400):
                #Triangle
                tri = Image.new('L', size, 255)
                draw = ImageDraw.Draw(tri, 'L')
                draw.polygon(((x1, y1), (x2, y2), (x3, y3)), fill=0, outline=0)
                tri.save('Imagenes/negro/Triangle/p{}.png'.format(i))

                tril = Image.new('L', size, 255)
                draw = ImageDraw.Draw(tril, 'L')
                draw.polygon(((x1, y1), (x2, y2), (x3, y3)), fill=255, outline=0)
                tril.save('Imagenes/negro/Triangle/pl{}.png'.format(i))

                trii = Image.new('L', size, 0)
                draw = ImageDraw.Draw(trii, 'L')
                draw.polygon(((x1, y1), (x2, y2), (x3, y3)), fill=255, outline=255)
                trii.save('Imagenes/blanco/Triangle/pi{}.png'.format(i))

                trili = Image.new('L', size, 0)
                draw = ImageDraw.Draw(trili, 'L')
                draw.polygon(((x1, y1), (x2, y2), (x3, y3)), fill=0, outline=255)
                trili.save('Imagenes/blanco/Triangle/pli{}.png'.format(i))
print ('listo')
