#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 27 23:27:06 2019

@author: cris
"""



from PIL import Image, ImageDraw

size = (128, 128)

eli = Image.new('L', size, 255)
draw = ImageDraw.Draw(eli, 'L')
draw.ellipse((0,0,70,70), fill=0, outline=0)
eli.save('Imagenes/elipses/elipse.png')
eli.show()

rec = Image.new('L', size, 255)
raw = ImageDraw.Draw(rec, 'L')
draw.rectangle([(0, 10), (20, 30), ()], fill=0, outline=0)
rec.save('Imagenes/rectangulos/rectangulo.png')
rec.show()


tri = Image.new('L', size, 255)
draw = ImageDraw.Draw(tri, 'L')
draw.polygon(((64, 0), (30,0), (0, 80)), fill=0, outline=0)
tri.save('Imagenes/triangulos/triangulo.png')
tri.show()