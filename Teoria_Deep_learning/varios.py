#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jun 23 22:02:56 2019

@author: cris
"""

oculto = "g fmnc wms bgblr rpylqjyrc gr zw fylb. rfyrq ufyr amknsrcpq ypc dmp. bmgle gr gl zw fylb gq glcddgagclr ylb rfyr'q ufw rfgq rcvr gq qm jmle. sqgle qrpgle.kyicrpylq() gq pcamkkclbcb. lmu ynnjw ml rfc spj."
#oculto = "http://www.pythonchallenge.com/pc/def/map.html"
oculto2 = "map"


def aclarar(oculto):
    abc = "abcdefghijklmnopqrstuvwxyz"
    claro=""
    for i in oculto:
        if i not in abc:
            claro = claro + i
        else:
            if abc.index(i) >= len(abc) - 2:
                claro = claro + abc[abc.index(i) - len(abc)+2]
            else:
                claro = claro + abc[abc.index(i) + 2]
    return claro
print(aclarar(oculto))
print(aclarar(oculto2))